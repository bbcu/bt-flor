.. _manual-main:

BT-FLOR: Build Transcripts From LOng Reads
==========================================

The software build transcripts from long reads.

The software get bam file as input and output gtf file with all alternative spliced transcripts and exons that declared by average of mapping positions of close reads.

The gtf also contain the counts of read in each transcript.

Optinally user can supply known gtf file. In this case the gtf also contains the percent of overlap between the known gtf to the closest transcript and exon that found by the BT-FLOR.

.. toctree::
    :maxdepth: 2

    rst/installation
    rst/user-guide
    rst/releases
    rst/source-code
    rst/algorithm

License
=======
BT-FLOR is licensed under GNU General Public License version 3. License needed for commercial use.

Author
======

Refael Kohen,

refael.kohen@weizmann.ac.il, refael.kohen@gmail.com

Bioinformatics unit at Life Sciences Core Facilities (LSCF)

Weizmann Institute of Science, Rehovot 76100, Israel.
