
Installation
############

*Support:* refael.kohen@weizmann.ac.il, refael.kohen@gmail.com


Requirements
============
* python 2.7

* python package: pysam==0.15.2

* installation insruction for pysam you can find here:
    `<https://pysam.readthedocs.io/en/latest/installation.html?highlight=installation#installation-from-repository>`_

    Example of installation of pysam with conda:
    ::

        conda create -n btflor python=2.7
        conda activate btflor
        conda config --add channels r
        conda config --add channels bioconda
        conda install pysam==0.15.2


Installation BT-FLOR package
============================

pip install bbcu.bt-flor



