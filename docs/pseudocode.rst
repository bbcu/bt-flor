Pseudocode of the project of BT_FLOR = Build Transcripts From Long Reads


Objects:
~~~~~~~~

"ReadsWithCloseStartMapping object":
====================================
average_start_position ("PositionAverage" object)
exons_start_positions (list of numbers that represent the start and end coordinate of each exon)
exons_end_positions (list of numbers that represent the start and end coordinate of each exon)
exons_start_average (list of PositionAverage object)
exons_end_average (list of PositionAverage object)


"PositionAverage" object:
=========================
average (average of the positions of the reads)
total_reads_in_pos (total number of reads in this position)
reads_names_in_pos (list of the reads name)








Procedures:
~~~~~~~~~~~

Main procedure:
===============
Input: reads are sorted by ReadsWithCloseStartMapping (sorted bam file).

For each read:
	1.	Find the closest "ReadsWithCloseStartMapping object" or create if not exist
	2.	If it is far away from current PositionAverage of the ReadsWithCloseStartMapping (or not exists any object):
			2.1 Run the procedure "find_positions_average" on current "ReadsWithCloseStartMapping object" with exons_start_positions.
			2.2 Run the procedure "find_positions_average" on current "ReadsWithCloseStartMapping object" with exons_end_positions.
			2.4 Run the procedure "build_transcripts" (the implement of the function including also the 2 above steps)
			2.3	Create a new object for current ReadsWithCloseStartMapping and add the read to the new object.
		Else: # implemented in ReadsWithCloseStartMapping.add_read()
			2.4	Belong the ReadsWithCloseStartMapping of the current read to the closest "ReadsWithCloseStartMapping object".
			2.5 Run the procedure "update_average_of_position" with read and "start position" of "ReadsWithCloseStartMapping object"


Procedure "build_transcripts":
==============================
input: "ReadsWithCloseStartMapping object", output file.

For each read in the object:
	1.	build transcript from "exons_X_average" lists by taking the averaged exons that it is belongs to them.

print to output file: the unique list of the transcripts and number of reads in each of them.


Procedure "find_positions_average" (in ReadsWithCloseStartMapping object):
==========================================================================
input: exons_X_positions (exons_start_positions or exons_end_positions), exons_X_average (exons_start_average or exons_end_average)

For each i position from the exons_X_positions  (dynamically changed within the loop):
	1.  Sort the reads (exons_X_positions) according the i position.

	2.  For each read in exons_X_positions in i position:
        2.1 Add a new PositionAverage object to exons_X_average list if not exists
        2.2 If i position in exons_X_positions of the read is close to i position in list of "exons_X_average:
2.2.1   Run procedure "update_average_of_position" with i position of read and i position of list of "exons_X_average"
        2.3 Else:
		    2.3.1   Move the i position of the read to i+1 location (shift the list)


Procedure "update_average_of_position" (in PositionAverage object):
===================================================================
Input: read (new reads to adding), read_position, PositionAverage object

1.	Change the PositionAverage according to the read position:
	average = read_position + average * total_reads_in_pos / (total_reads_in_pos + 1)
2.	Update the number of reads and list of reads name of PositionAverage object with the current read




